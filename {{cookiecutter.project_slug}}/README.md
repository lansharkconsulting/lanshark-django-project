LANshark Django Project
=======================

Goals:

 1) Use nginx/supervisor or systemd
 2) use PDM for package manaagement (currently incompatible with asdf-python)
 3) use asdf/direnv for virtual environment
 4) support python 3.10
 5) support latest django (4.x)
 6) rich base package
 7) support Bootstrap 5
 8) separate version for wagtail
 9) support webpack/babel/npm/nodejs
10) move /admin to /backend, and install honeypot
11) support 'maintenance mode' via Nginx
12) configured for use with cloudflare (load balance)
13) make this a cookiecutter
14) Use pre-commit hooks
15) Convert to Email as default user (allauth)
16) Add better .gitignore and .gitattributes

# Directory Definitions

PROJECT_ROOT = Project home directory (where settings.py is)
BASE_DIR = manage.py home directory
HTDOCS_ROOT = PROJECT_ROOT  (static and media files locally)

# Running locally

To get a local copy of the server running, use docker-compose to run the
backing services (Postgresql, MailHog, Redis, etc).  Create a virtualenv (I
prefer asdf/direnv) for the project, and install all the packages with pdm
install. Then run the main application as usual with manage.py.

To configure the system, copy the env-example file to .env, and edit for your
local situation.  THEORETICALLY, you will not need a .env for a production
environment, though there may be some values which do not have a default
(SECRET_KEY, etc.).  IF you do not define those values in the production
environment, the app will not run.

There is an environment variable CONTEXT defined as 'local', 'staging', or 'prod'.
You may use this variable to conditionally load or change settings based on
if it's local development, or dev/staging, or production.  DO NOT create
separate settings files.

OS Package Requirements
-----------------------

    libjpeg provides JPEG functionality.
    zlib provides access to compressed PNGs
    libtiff provides compressed TIFF functionality
    libfreetype provides type related services
    littlecms provides color management
    libwebp provides the WebP format.
    tcl/tk provides support for tkinter bitmap and photo images.
    openjpeg provides JPEG 2000 functionality.
    libxcb provides X11 screengrab support.


Packages to include (R=Required, O=optional)
--------------------------------------------
Legend:

  +: installed and configured
  -: installed, not yet configured
  ?: needs work to be compatible
  #: not installed, but worth remembering

+bumpversion (R)  (replace with pdm bump?)
#celery (O)
+django (R)
+django-admin-env-notice (R)
#django-allauth (R)
#django-anymail (R)  (with mailgun)
-django-autocomplete-light (O)
+django-braces (R)
-django-cities-light (O)
-django-countries (O)
+django-crispy-forms (R)
?django-dbbackup (O) - This is buggy for PostgreSQL... (try .pgpass) (not compatible with DJ 4.x)
+crispy-bootstrap5 (R)
-django-encrypted-filefield (O)
-django-encrypted-model-fields (O)
+django-model-utils (R)
#django-phonenumber-field (O)
#django-redis (R)
#django-waffle (O)
+pendulum (R)
+Pillow (R)
+psycopg2 (R)
+python-decouple (R)
+pytz (R)  Is this still Necessary?
#phonenumbers (R)
#phonenumberslite (R)
#rules (O)
+sentry-sdk (O)
supervisor (R)  (optional systemd?)
+whitenoise[brotli] (R)


Development
-----------
+django-debug-toolbar (R)
+django-extensions (R)
+flake8 (R)
-pycodestyle (R)
-pyflakes (R)
+pylint (R)
+Sphinx (R)
+Werkzeug (R)


Testing
-------
coverage (R)
django-coverage-plugin (R)
django-test-plus (O)
factory_boy (R)
Faker (O)
mock (O)
pep8 (R)
pytest (R)
pytest-cookies (R)
pytest-django (R)
pytest-factoryboy (R)
pytest-sugar (R)
tox (R)


Production
----------
boto3 (O)
django-storages (O)


Possible packages to add
------------------------
django-admin-bootstrap
django-admin-extensions
django-admin-import
django-admin-shortcuts
django-analytical
django-brutebuster
django-caching-app-plugins
django-classy-tags
django-compat
django-compat-lint
django-fixture-magic
django-floppyforms
django-ganalytics
django-keyedcache3
django-livesettings
django-money  (not supported above django 3.2, python 3.9)
django-ordered-model
django-post_office
django-profiletools
django-redis-cache
django-reversion
django-robots
django-secure
django-sekizai
django-smuggler
django-templated-email
django-templatetag-sugar
django-webpack-loader
django-widget-tweaks
easy-thumbnails
html5lib
pycrypto
py-moneyed
python-dateutil
requests
snakeviz
sorl-thumbnail
progressbar2

NewRelic Integration...
